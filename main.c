#include <stdio.h>
#include "unity.h"
#include "estatistica.h"

void setUp() 
{
}

void tearDown() 
{
}
//testa valor correto de resposta. Teste retorna aprovado como esperado
void test_media_certo() 
{ double v[] = { 1, 2};
  TEST_ASSERT_EQUAL(media(2,v),1.5);
}
//testa valor errado de resposta. Teste retorna falha como esperado
void test_media_errado() 
{
double v[] = { 2, 2};
  TEST_ASSERT_EQUAL(media(2,v),8);
}
//testa se aceita a parte inteira da resposta. Deveria retornar resposta de falha
void test_media_inteira() 
{
double v[] = { 5, 2};
  TEST_ASSERT_EQUAL(media(2,v),3);
}
//testa se aceita a resposta decimal. Deve retornar aprovada
void test_media_decimal() 
{
double v[] = { 5, 2};
  TEST_ASSERT_EQUAL(media(2,v),3.5);
}
//testa função media com valor negativo. Deve retornar FAIL
void test_media_negativo() 
{
double v[] = { -5, -2};
  TEST_ASSERT_EQUAL(media(2,v),3.5);
}
//testa função media com valor negativo. Deve retornar aprovado
void test_media_negativo_correto() 
{
double v[] = { -5, -2};
  TEST_ASSERT_EQUAL(media(2,v),-3.5);
}
//testa valor correto de resposta. Teste retorna aprovado como esperado
void test_desvio_certo() 
{ double v[] = { 6,2,3,1};
  TEST_ASSERT_EQUAL(desvio(4,v),1.87);
}
//testa valor errado de resposta. Teste retorna falha como esperado
void test_desvio_errado() 
{
double v[] = { 6, 2,3,1};
  TEST_ASSERT_EQUAL(desvio(4,v),8);
}
//testa se aceita a parte inteira da resposta. Deveria retornar resposta de falha
void test_desvio_inteira() 
{
double v[] = { 6,2,3,1};
  TEST_ASSERT_EQUAL(desvio(4,v),1);
}
//testa se aceita a resposta decimal. Deve retornar aprovada
void test_desvio_decimal() 
{
double v[] = { 6, 2,3,1};
  TEST_ASSERT_EQUAL(desvio(4,v),1.87);
}
//testa função desvio com valor negativo. Deve retornar FAIL
void test_desvio_negativo() 
{
double v[] = { -5, -2};
  TEST_ASSERT_EQUAL(desvio(2,v),-3.5);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_media_certo);
  RUN_TEST(test_media_errado);
  RUN_TEST(test_media_inteira);
  RUN_TEST(test_media_decimal);
  RUN_TEST(test_media_negativo);
  RUN_TEST(test_media_negativo_correto);
  RUN_TEST(test_desvio_certo);
  RUN_TEST(test_desvio_errado);
  RUN_TEST(test_desvio_inteira);
  RUN_TEST(test_desvio_decimal);
  RUN_TEST(test_desvio_negativo);
  
  return UNITY_END();
}
