#include <stdio.h>
#include "estatistica.h"
#include <math.h>
double media(int n, double *v)
{

double soma = 0; //ponto de partida
  for(int i=0; i<n; i++)
  {
   soma = soma + v[i];
  }
  double z;
  z = soma/n;
return z;
}

double desvio(int n, double *v)
{
int i;
double var;
double respo = 0;
double media,desvio,soma=0;
for(i=0; i<n; i++)
{
  soma += v[i];  
}
media = soma/n;

for(i = 0; i< n; i++)
{
 respo = respo + ((v[i] - media)*(v[i] - media));

}
var = sqrt(respo/n-1);
return var;

}
